# Backups

The backup files in this repository are utterly useless other then that they
contain the initial expected setup as required by `apex-master`, and
`apex-configure`. The structure should be created by the projects though, this
will hopefully be changed at some future time.

## Setup

### Fedora

```sh
dnf install -y mongo-tools postgresql
```

## PostgreSQL

### Backup

Assuming the host being backed up is `localhost`.

```sh
pg_dump -h localhost -U postgres -p 5432 -F c -b -v -f backup/postgres/pg.dump postgres
```

### Recovery

If the PostgreSQL database being recovered to is using the networking from the
`docker-compose.yml` file in this repository.

```sh
pg_restore -h 172.28.1.11 -U postgres -p 5432 -d postgres -v backup/postgres/pg.dump
```

## MongoDB

### Backup

Assuming the host being backed up is `localhost`.

```sh
mongodump --host localhost --port 27017 --db apex backup/mongodb/dump
```

### Recovery

If the MongoDB database being recovered to is using the networking from the
`docker-compose.yml` file in this repository.

```sh
mongorestore --host 172.28.1.12 --port 27017 backup/mongodb/dump
```
